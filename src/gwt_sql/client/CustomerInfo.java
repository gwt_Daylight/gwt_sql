package gwt_sql.client;

import java.io.Serializable;

import gwt_sql.shared.DBRecord;

public class CustomerInfo implements Serializable {

	private static final long serialVersionUID = -713790906895544173L;

	private DBRecord customerInfo = new DBRecord();

	public CustomerInfo() {
	}

	public void setCustomerInfo(DBRecord rec) {
		customerInfo = rec;
	}

	public DBRecord getCustomerInfo() {
		return customerInfo;
	}
}